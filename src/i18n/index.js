import Vue from "vue"
import Vuex from "vuex"

import vuexI18n from "vuex-i18n"

Vue.use(Vuex)

const store = new Vuex.Store()

Vue.use(vuexI18n.plugin, store)

Vue.i18n.add("br", require("./pt_br.json"))

Vue.i18n.set("br")
Vue.i18n.fallback("br")
