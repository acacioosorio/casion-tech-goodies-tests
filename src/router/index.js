import Vue from 'vue'
import VueRouter from 'vue-router'

import AppLayout from "@/components/layouts/app-layout/AppLayout"
const home = () => import('@/views/home')

Vue.use(VueRouter)

export default new VueRouter({
	routes: [{
			path: '*',
			redirect: {
				name: 'index'
			},
		},
        {
            path: "/",
            redirect: "/home",
            component: AppLayout,
            children: [
                {
					name: 'home',
					path: '/',
					component: home,
				}
            ]
        }
	]
})