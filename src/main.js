import Vue from 'vue'
import App from './App.vue'
import store from "./store"
import router from './router'
import "./i18n"

import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'

import PerfectScrollbar from 'vue2-perfect-scrollbar'
import 'vue2-perfect-scrollbar/dist/vue2-perfect-scrollbar.css'

Vue.config.productionTip = false
Vue.use(PerfectScrollbar)

new Vue({
  store,
  router,
  render: h => h(App),
}).$mount('#app')
