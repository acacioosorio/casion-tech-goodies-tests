import axios from 'axios'

const baseDomain = process.env.SPOTIFY_APP_API_URL || "http://demo3489056.mockable.io"

const client = axios.create({
    baseURL: baseDomain,
    headers: {
        "Content-Type": "application/json",
        "Accept": "application/json",
        "Content-Language": `pt-BR`
    }
})

export default client