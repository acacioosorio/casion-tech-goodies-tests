import Http from "./http"

// RECENT => https://run.mocky.io/v3/fffd25ca-b1dd-4306-ad03-879e35c47950

export const allPlaylists = () => Http.get('/playlists')

export const recentlyPlayed = () => Http.get('/recently-played')