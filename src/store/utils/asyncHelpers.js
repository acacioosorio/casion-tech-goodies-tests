export const createAsyncMutation = (type) => ({
	SUCCESS: `${type}_SUCCESS`,
	FAILURE: `${type}_FAILURE`,
	PENDING: `${type}_PENDING`,
})

export const performAsyncAction = (store, request, types, ...args) => {
	store.commit(types.PENDING)

	return Promise.resolve(request)
		.then(({
			data
		}) => store.commit(types.SUCCESS, {
			...data,
			optional: args
		}))
		.catch(({
			response
		}) => store.commit(types.FAILURE, {
			...response,
			optional: args
		}))
}