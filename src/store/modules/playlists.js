import { allPlaylists } from '@/services/playlistsService'
import {createAsyncMutation, performAsyncAction} from '../utils/asyncHelpers'

const allPlaylistsAsync = createAsyncMutation('ALL_PLAYLISTS')

const state = {
    requests: {
        allPlaylistsAsync: ''  
    },
    homePlaylists: [],
    basedOnYourListened: [],
    bestOfArtist: [],
    goodDreams: [],
    hundredMadeForYou: [],
    madeForYou: [],
    recentlyPlayed: [],
    recommendedForToday: [],
    tops: [],
    yourFavoriteAnimes: [],
    errors: []
}

const getters = {
    basedOnYourListened: (getterState) => {
        return getterState.basedOnYourListened
    },
    bestOfArtist: (getterState) => {
        return getterState.bestOfArtist
    },
    goodDreams: (getterState) => {
        return getterState.goodDreams
    },
    hundredMadeForYou: (getterState) => {
        return getterState.hundredMadeForYou
    },
    madeForYou: (getterState) => {
        return getterState.madeForYou
    },
    recentlyPlayed: (getterState) => {
        return getterState.recentlyPlayed
    },
    recommendedForToday: (getterState) => {
        return getterState.recommendedForToday
    },
    tops: (getterState) => {
        return getterState.tops
    },
    yourFavoriteAnimes: (getterState) => {
        return getterState.yourFavoriteAnimes
    },
    homePlaylists: (getterState) => {
        return getterState.homePlaylists
    }
}

const mutations = {
    [allPlaylistsAsync.PENDING](state) {
        state.requests.allPlaylistsAsync = 'pending'
    },
    [allPlaylistsAsync.SUCCESS](state, response) {

        state.requests.allPlaylistsAsync = 'success'
        
        state.homePlaylists = response.data.items
    },
    [allPlaylistsAsync.FAILURE](state, response) {
        state.errors = []
        let errors = response

        if(Array.isArray(errors)) state.errors = errors
        if(typeof errors === 'string') state.errors.push({description: errors})

        state.requests.allPlaylistsAsync = 'failure'
    },
}

const actions = {
    allPlaylistsAsync: async(store, params) => await performAsyncAction(store, allPlaylists(params), allPlaylistsAsync)
}

export default { state, mutations, actions, getters }