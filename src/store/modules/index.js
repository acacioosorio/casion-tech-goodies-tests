const requireModule = require.context(".", false, /\.js$/)
const modules = {}

requireModule.keys().forEach(fileName => {
    if (fileName === "./index.js") return

    // filter fullstops and extension
    // and return a camel-case name for the file
    const moduleName = fileName.replace(/(\.\/|\.js)/g, "")
    // create a dynamic object with all modules
    modules[moduleName] = {
        namespaced: true,
        ...requireModule(fileName).default
    }
})
export default modules